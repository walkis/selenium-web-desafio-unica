Feature: Catalog

	Background: 
  
  		Given user loads the configuration data

	Scenario Outline: return products list of catalog accord on the item searched
	
	Given user navigates to '<site>'
	When user search 'txtSearch' 'notebook'
		And user clicks 'btnSearch'
	Then it should return 'items' for '<result>'
		And quit driver	

	Examples:
    |site						|result		|
    |https://www.amazon.com.br/ |notebook	|

	Scenario Outline: message product not found in catalog
	
	Given user navigates to '<site>'
	When user search 'txtSearch' '.@#@#@#%@53235jasdjlkasjd'
		And user clicks 'btnSearch'	
	Then it should return 'itemNoFound' for '<result>'	
		And quit driver

	Examples:
    |site						|result		|
    |https://www.amazon.com.br/ |.@#@#@#%@53235jasdjlkasjd	|