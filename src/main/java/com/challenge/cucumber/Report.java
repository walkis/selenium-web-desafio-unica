package com.challenge.cucumber;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.ReportBuilder;

public class Report {
	
	public static void GenerateCucumberReport() {
			File reportOutputDirectory = new File("target");
			String filepath = "./target/cucumber-report.json";

			List<String> jsonFiles = new ArrayList<String>();
			try {
				jsonFiles.add(new File(filepath).getCanonicalPath());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			String projectName = null;
			try {
				projectName = new File(".").getCanonicalPath()
						.substring(new File(".").getCanonicalPath().lastIndexOf("/") + 1);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			String buildNumber = "1";
			String project_name = String.format("cucumber-%s", projectName);
			boolean runWithJenkins = false;
			boolean parallelTesting = false;

			Configuration configuration = new Configuration(reportOutputDirectory, project_name);
			// optional configuration
			configuration.setParallelTesting(parallelTesting);	
			configuration.setRunWithJenkins(runWithJenkins);
			configuration.setBuildNumber(buildNumber);

			ReportBuilder reportBuilder = new ReportBuilder(jsonFiles, configuration);
			reportBuilder.generateReports();
		}
}
