package com.challenge.common;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.codehaus.plexus.util.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

public class OperatorsBase extends DriverManager {
	
	public void openURL(String url){				
		getDriver().navigate().to(url);
		TakeScreenShot();
	}
	
	public void onClicks(String elementName) throws InterruptedException{		
		Thread.sleep(1000);		
		getIOData().getElementNode(elementName);
		switch (getIOData().getTypeLocator()) {
		case "id":
			getDriver().findElement(By.id(getIOData().getValueLocator())).click();
			break;
			
		case "xpath":
			getDriver().findElement(By.xpath(getIOData().getValueLocator())).click();
			break;

		case "class":
			getDriver().findElement(By.className(getIOData().getValueLocator())).click();
			break;
			
		default:
			break;
		}				
	}

	private static String contextOfProduct;

	public void setContextOfProduct(String elementName) throws Exception {		
				Thread.sleep(500);
				getIOData().getElementNode(elementName);		
				OperatorsBase.contextOfProduct = getDriver().findElement(By.xpath(getIOData().getValueLocator())).getText();						
			}

	public static String getContextOfProduct() {
		return contextOfProduct;
	}
			
	
	public void Scroll(String scrollType) throws InterruptedException{	
		Thread.sleep(3000);
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		js.executeScript("window.scrollBy(0,450)", "");
	}
	
	public void ScrollUntil(String scrollType, String elementName) throws InterruptedException{	
		getIOData().getElementNode(elementName);		
		((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", getDriver().findElement(By.xpath(getIOData().getValueLocator())));
		Thread.sleep(1000); 
	}
	
	public void TakeScreenShot(){
		File scrFile = ((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(scrFile, new File("./res_out/evidence/screenshot"+new Date()+".png"));			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	

	private static IOFile IOData;
	
	public IOFile getIOData() {
		return IOData;
	}

	public void setIOData(IOFile ioFile) {
		this.IOData = ioFile;
	}	
}
