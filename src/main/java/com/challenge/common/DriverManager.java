package com.challenge.common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DriverManager {	
	public void Browser(String browser){
		System.out.println("browser: " + browser);
		if (browser.toLowerCase().toString().equals("chrome")) {			
			WebDriverManager.chromedriver().setup();
			setDriver(new ChromeDriver());
		}		
	}
	
	private static WebDriver driver;
	
	public WebDriver getDriver() {
		return driver;
	}

	private void setDriver(WebDriver driver) {
		DriverManager.driver = driver;
	}
}
