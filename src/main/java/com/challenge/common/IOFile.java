package com.challenge.common;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class IOFile {	

	private NodeList nodeList;
	
	public NodeList getNodeList() {
		return nodeList;
	}

	public void setNodeList(NodeList nodeList) {
		this.nodeList = nodeList;
	}

	private File apkFile;
	
	public File getApkFile() {
		return apkFile;
	}

	public void setApkFile(File apkFile) {
		this.apkFile = apkFile;
	}

	private String keyLocator;
	private String typeLocator;
	private String valueLocator;
	
	public String getKeyLocator() {
		return keyLocator;
	}

	public void setKeyLocator(String keyLocator) {
		this.keyLocator = keyLocator;
	}

	public String getTypeLocator() {
		return typeLocator;
	}

	public void setTypeLocator(String typeLocator) {
		this.typeLocator = typeLocator;
	}

	public String getValueLocator() {
		return valueLocator;
	}

	public void setValueLocator(String valueLocator) {
		this.valueLocator = valueLocator;
	}

	private Map<String, String> properties;

	public Map<String, String> getProperties() {
		return properties;
	}

	public void setProperties(Map<String, String> properties) {
		this.properties = properties;
	}	
	
	public Map<String, String> readPropertiesFile(String location) throws Exception {

	    Map<String, String> properties = new HashMap<>();
	    Properties props = new Properties();
	    props.load(new FileInputStream(new File("./res_out/"+location.substring(2))));
	    props.forEach((key, value) -> {
	        properties.put(key.toString(), value.toString());
	    });
	    
	    setProperties(properties);	    
	    loadXMLFile();
//	    loadAPKFile();	    
	    return properties;
	}	
	
	private void loadXMLFile(){		
		 try {
			File xmlFile = new File("./res_out/"+getProperties().get("filepath_uimap").substring(2));
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlFile);
			doc.getDocumentElement().normalize();
			
			System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
			setNodeList(doc.getElementsByTagName("locator"));
			System.out.println("----------------------------");
		    } catch (Exception e) {
		    	e.printStackTrace();
		    }
		}
	
	public void getElementNode(String elementKey) {		
		for (int temp = 0; temp < getNodeList().getLength(); temp++) {
			Node node = getNodeList().item(temp);
//			System.out.println("\nCurrent Element :" + node.getNodeName());	
			Element element = (Element) node;			
			if (element.getAttribute("key").equals(elementKey)) {		
				setTypeLocator(element.getAttribute("type"));
				setValueLocator(element.getAttribute("value"));
			}
		}		
	}
}
