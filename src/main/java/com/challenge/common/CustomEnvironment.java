package com.challenge.common;

public class CustomEnvironment extends OperatorsBase{	

	public void loadConfiguration() throws Exception {
		setIOData(new IOFile());
		getIOData().readPropertiesFile("./Web.properties");		
		Browser(getIOData().getProperties().get("browser"));	    		
	}
}
