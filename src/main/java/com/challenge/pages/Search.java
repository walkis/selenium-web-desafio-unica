
package com.challenge.pages;

import java.io.IOException;

import org.junit.Assert;
import org.openqa.selenium.By;

import com.challenge.common.OperatorsBase;

public class Search extends OperatorsBase{

	public void userSearchFrom(String elementName, String text) throws Exception {		
//		Thread.sleep(3000);
		getIOData().getElementNode(elementName);		
		getDriver().findElement(By.id(getIOData().getValueLocator())).sendKeys(text);		
		TakeScreenShot();
	}
	
	public void printItemsFromSearch(String elementNameA, String expected) throws InterruptedException, IOException{
		Thread.sleep(2000);
		TakeScreenShot();
		getIOData().getElementNode(elementNameA);					
		Assert.assertTrue(" list of items return in catalog: ", 
			getDriver().findElement(By.xpath(getIOData().getValueLocator())).getText().toLowerCase().contains(expected.toLowerCase()));		
	}

	public void printDetailsOfItemFromCart(String elementNameA) throws InterruptedException, IOException{
		Thread.sleep(2000);
		TakeScreenShot();
		getIOData().getElementNode(elementNameA);			
		Assert.assertTrue(" details of item in cart: ", 
			getDriver().findElement(By.xpath(getIOData().getValueLocator())).getText().toLowerCase().contains(getContextOfProduct().toLowerCase()));		
	}

	public void printSubTotalOfItemFromCart(String elementNameA) throws InterruptedException, IOException{
		Thread.sleep(2000);
		TakeScreenShot();
		getIOData().getElementNode(elementNameA);			
		Assert.assertTrue(" sub total of item in cart: ", 
		Double.parseDouble(getContextOfProduct().toLowerCase().replace("r$","").replace("\n","")) * 2 ==
		Double.parseDouble(getDriver().findElement(By.className(getIOData().getValueLocator())).getText().toLowerCase()));		
	}
}