package com.challenge.main;

import java.io.IOException;

import com.challenge.cucumber.*;

public class Main {

	public static void main(String[] args) throws IOException {
		// default arguments
		String[] myArgs = { 
				"--plugin", "json:target/cucumber-report.json"
		};
		
		System.out.println("Used arguments:");
		for (String arg : myArgs) {
			System.out.print(arg + " ");
		}
		System.out.println("\n");

		// call cucumber api with arguments
		byte exitstatus = cucumber.api.cli.Main.run(myArgs, Thread.currentThread().getContextClassLoader());

		// generate report before close execution
		Report.GenerateCucumberReport();
		
		//close execution
		System.exit(exitstatus);
	}
}
