package com.challenge.steps;

import com.challenge.common.CustomEnvironment;
import com.challenge.common.OperatorsBase;
import com.challenge.pages.Search;

import cucumber.api.java.en.*;

public class StepsDefinitions extends OperatorsBase {
    @Given("^user loads the configuration data$")
	public void userLoadsTheConfigurationData() throws Throwable {
		new CustomEnvironment().loadConfiguration();		
	}

    @Given("^user navigates to '(.*)'$")
	public void userNavigatesTo(String arg) throws Throwable {		
		new OperatorsBase().openURL(arg);
	}
	
	@Given("^user clicks '(.*)'$")
	public void userClicks(String elementName) throws Throwable {
		onClicks(elementName);
	}	

    @When("^user search '(.*)' '(.*)'$")
	public void userSearch(String elementName, String text) throws Throwable {		
		new Search().userSearchFrom(elementName, text);
	}	

	@When("^get details of product '(.*)'$")
	public void detailsOfProduct(String elementName) throws Throwable {		
		setContextOfProduct(elementName);
	}	
	
	@When("^user scroll '(.*)'$")
	public void userScroll(String scrollType) throws Throwable {		
		Scroll(scrollType);	
	}
	
	@When("^user scroller '(.*)' until '(.*)'$")
	public void userScrollUntil(String scrollType, String elementName) throws Throwable {
		ScrollUntil(scrollType, elementName);	
	}
	
	@When("^quit driver$")
	public void quitDriver() throws Throwable {		
		getDriver().quit();
	}   

	@Then("^it should return '(.*)' for '(.*)'$")
	public void itShouldReturnItems(String elementName, String expectContext) throws Throwable {		
		new Search().printItemsFromSearch(elementName, expectContext);			
	}

	@Then("^it should return '(.*)' cart$")
	public void itShouldReturnItemPrice(String elementName) throws Throwable {		
		new Search().printDetailsOfItemFromCart(elementName);			
	}

	@Then("^it should return subTotal '(.*)' cart$")
	public void itShouldReturnItemSubTotal(String elementName) throws Throwable {		
		new Search().printSubTotalOfItemFromCart(elementName);			
	}
}
