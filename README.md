

Projeto Testes Automatizado:

- java
- selenium
- maven
- bdd

Evidências
- report ../target/cucumber-html-reports/step-overview.html
- evidence ... screenshot

Melhorias:

- organizar as evidências por cenário, remover a pasta evidencia a cada regressão
- ajustar timewait para que seja conforme o load da página
- deixar a estrutura dos testes na feature mais flexível p/ não ter que criar muitos steps
- tornar comuns os parâmetros de entrada e saída dentro da feature
- separar as features de acordo com a funcionalidade
- separar as classes por funcionalidades
- ajustar o reporte dos testes, tentar outra opção como serenity
- a captura de elementos específico para validar o resultado
- deixar a class getContext para trabalhar conforme o tipo de tag id, class, xpath ...
- adicionar outras validações no cart e demais páginas, total do produto, adição de mais itens no cart...
- ajustar processo para fechar o driver independente da falha
- adicionar no gitignore os diretórios e arquivos desnecessários