Feature: Adicionar Produto no Cart

	Background: 
  
  		Given user loads the configuration data
   
    Scenario Outline: add product to cart and check out your details
	
	Given user navigates to '<site>'
	When user search 'txtSearch' 'notebook'
		And user clicks 'btnSearch'
		And user scroll 'Down'
		And get details of product 'priceOfItemCatalog' 	
		And user clicks 'selectProduct'	
	Then it should return 'titleOfItemCart' for '<result>'
	Then it should return 'priceOfItemCart' cart
		And quit driver

	Examples:
    |site						|result		|
    |https://www.amazon.com.br/ |notebook	|	

	Scenario Outline: add dois products to cart
	
	Given user navigates to '<site>'
	When user search 'txtSearch' 'notebook'
		And user clicks 'btnSearch'				 	
		And user scroll 'Down'
		And user clicks 'selectProduct'	
		And get details of product 'priceOfItemCart'
		And user clicks 'selectQuantityOfItemCart'
		And user clicks 'btnAddCart'
	Then it should return subTotal 'subTotalOfItemCart' cart
		And quit driver

	Examples:
    |site						|result		|
    |https://www.amazon.com.br/ |notebook	|		

